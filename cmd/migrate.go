package cmd

import (
	"ff/webserver/global"
	"ff/webserver/initialize"
	"ff/webserver/model"
	"fmt"
	"github.com/spf13/cobra"
)

var migrateCmd = &cobra.Command{
	Use:   "migrate",
	Short: "use this command to migrate your db",
	Long: ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("start migrating database")
		// 初始化数据库
		initialize.InitMysql()
		// 迁移结束关闭数据库
		defer func() {
			if err := global.DB.Close(); err != nil{
				fmt.Println("close db failed:", err)
			}
		}()
		global.DB.AutoMigrate(&model.Bookmark{}, &model.Memo{}, &model.User{})
		fmt.Println("migrate complete!")
	},
}
