package cmd

import (
	"ff/webserver/global"
	"ff/webserver/initialize"
	"fmt"
	"net/http"
	"time"

	"github.com/spf13/cobra"
)

var S = &http.Server{}

var webServerCmd = &cobra.Command{
	Use:   "webserver",
	Short: "use this command to start ff web server",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
	},
}

func init() {
	webServerCmd.AddCommand(startWebServerCmd)
}

var startWebServerCmd = &cobra.Command{
	Use:   "start",
	Short: "use this command to start web server",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		startWebServer()
	},
}

func startWebServer() {
	// 加载配置文件, 此步骤放在第一步！！！
	initialize.InitViper()

	// 初始化日志记录器
	initialize.InitLogger()

	// 初始化路由
	rootRouter := initialize.InitRouter()

	// 初始化数据库配置
	initialize.InitMysql()

	// 程序结束前关闭数据库连接
	defer func() {
		if err := global.DB.Close(); err != nil{
			global.Logger.Error(fmt.Sprintln("close db failed: " ,err))
		}
	}()

	// HTTP配置
	S = &http.Server{
		Addr:           fmt.Sprintf(":%d", global.VP.GetInt("server.http-port")),
		Handler:        rootRouter,
		ReadTimeout:    global.VP.GetDuration("server.read-timeout") * time.Second,
		WriteTimeout:   global.VP.GetDuration("server.write-timeout") * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	global.Logger.Info("==================== start ff ... ====================")
	if err := S.ListenAndServe(); err != nil {
		global.Logger.Error(err.Error())
	}
}
