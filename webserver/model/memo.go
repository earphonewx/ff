package model

import (
	"ff/webserver/global"
	"gorm.io/gorm"
	"time"
)

type Memo struct {
	gorm.Model
	Content  string    `gorm:"type:varchar(100); NOT NULL" json:"content"`
	Deadline time.Time `gorm:"NOT NULL" json:"deadline"`
}

func GetMemo(offset int, limit int, maps interface{}) (res []Memo, err error) {
	err = global.DB.Model(&Memo{}).Order("deadline desc").
		Where(maps).Offset(offset).Limit(limit).Find(&res).Error

	return
}

func GetMemoCount(maps interface{}) (count int, err error) {
	err = global.DB.Model(&Memo{}).Where(maps).Count(&count).Error

	return
}

func AddMemo(memo interface{}) (err error) {
	// 防止前端指定id创建
	err = global.DB.Omit("id").Create(memo).Error

	return
}

func EditMemo(id uint, data interface{}) (err error) {
	err = global.DB.Model(&Memo{}).Where("id = ?", id).Omit("id").Updates(data).Error

	return
}

func DeleteMemo(id int) (err error) {
	err = global.DB.Where("id = ?", id).Delete(&Memo{}).Error
	return
}
