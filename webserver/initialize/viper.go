package initialize

import (
	"ff/webserver/global"
	"fmt"
	"github.com/spf13/viper"
	"os"
)

func InitViper() {
	global.VP = viper.New()
	global.VP.SetConfigType("yaml")
	global.VP.AddConfigPath("./setting")
	// global.VP.AddConfigPath(".")

	if _, err := os.Stat("./setting/config.yaml"); err == nil {
		global.VP.SetConfigName("config")
	} else {
		global.VP.SetConfigName("config-example")
	}

	if err := global.VP.ReadInConfig(); err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
	fmt.Println("initialize system config success!")
}
