package initialize

import (
	"ff/webserver/global"
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"os"
	"time"
)

func InitMysql() {
	db, err := gorm.Open("mysql",
		fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local",
			global.VP.GetString("mysql.user"),
			global.VP.GetString("mysql.password"),
			global.VP.GetString("mysql.host"),
			global.VP.GetInt("mysql.port"),
			global.VP.GetString("mysql.db-name")))

	if err != nil {
		global.Logger.Error(fmt.Sprintln("init mysql failed: ", err.Error()))
		os.Exit(0)
	}

	gorm.DefaultTableNameHandler = func(db *gorm.DB, defaultTableName string) string {
		return defaultTableName
	}

	// 全局禁用表名复数
	db.SingularTable(true)

	// 开启日志
	db.LogMode(global.VP.GetBool("mysql.enable-sql-log"))

	// SetMaxIdleCons 设置连接池中的最大闲置连接数。
	db.DB().SetMaxIdleConns(global.VP.GetInt("mysql.max-idle-conn"))

	// SetMaxOpenCons 设置数据库的最大连接数量。
	db.DB().SetMaxOpenConns(global.VP.GetInt("mysql.max-open-conn"))

	// SetConnMaxLifetime 设置连接的最大可复用时间。
	db.DB().SetConnMaxLifetime(time.Duration(global.VP.GetInt("mysql.conn-max-lifetime")) * time.Minute)

	global.DB = db
}
