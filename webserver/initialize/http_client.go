package initialize

import (
	"ff/webserver/global"
	"net/http"
)

func InitHttpClient() {
	global.HttpClient = &http.Client{}
}
